# Quick Tutorial on Configuring Destiny2Extractor on Windows 10

Hello and welcome to the advanced data mining tutorial. 
<br>In the following tutorial you can see which software is required and how to set it up.
<br>If you are not so talented in software programming, then go directly to the third point.

This tutorial will be expanded soon as I gather more insight. 
<br>That's why this tutorial initially only refers to the extraction of the audio files from Destiny 2. 

Have fun and good luck with data mining.


## Table of Contents
**[1]** Prerequisites<br>
**[2]** Environment set up<br>
**[3]** Compiling Destiny2Extractor<br>
**[4]** Audio file extraction

<br>

### Prerequisites

Before we can set up our development environment, the following software must be downloaded and installed.

> Tip | It isn`t always good to install software, so you can download these packages (CLion & Notepad++) as a portable version.

```
- Visual Studio Community 2017
- CLion
- Notepad++ (incl. HexEditor)
- ww2ogg
```

Before you start downloading all the programs, head over to the next point.<br>
In the next chapter you will find all direct download links via the official manufacturer page.

<br>

### Environment set up

> **Warning**: All posted links have been shorten by Bitly!

a) **Visual Studio Community 2017** | Direct link: https://bit.ly/2KOgBab<br>
Just start the installer and when it asks for configuration, then use the following configuration.<br>
It's just the minimum for our development environment and can be extended later.<br>
![VS Community 2017 configuration](https://i.imgur.com/vAyh996.png)<br>
    
b) **CLion v2018.1** | Direkt link: https://bit.ly/2JdZ71D
    
`CLion is not a free of use software! In this case just activate the 30 days trial version.`

#### Quick Tutorial on Configuring CLion on Windows

In addition to MinGW or Cygwin as operating environments for CLion, you can try using Microsoft Visual C++ compiler. CLion supports that compiler with CMake and NMake generator. With Microsoft Visual C++ you are able to work with the various cross-platform projects which often use Microsoft Visual Studio under Windows as a working environment.

To begin with, you need either Visual Studio 2013, 2015 or 2017 to be installed on your system. Having that, configure CLion to use Visual C++ compiler:

Set up Microsoft Visual C++ compiler as your environment.
Go to the Toolchains settings dialog (**File | Settings | Build, Execution, Deployment | Toolchains**) and select **Visual Studio** from the list of available environments. Note, that CLion automatically detects the Visual Studio version installed on your system.<br>
![CLion v2018.1 Adding Toolchain](https://i.imgur.com/PTZTzJ3.png)<br>
- Specify the architecture (x86, amd64, x86_arm, amd64_arm, etc), type of a platform (store or uwp or leave it blank) and version (Windows SDK name or leave it blank).
- That's all. Now you can build your project with MSVC and run it. Note, that CLion will run CMake with the NMake generator in this case.